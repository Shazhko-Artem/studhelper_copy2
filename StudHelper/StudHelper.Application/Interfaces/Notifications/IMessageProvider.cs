﻿namespace StudHelper.Application.Interfaces.Notifications
{
    public interface IMessageProvider
    {
        string GetMessage(EmailMessageType type, string callBackUrl);
    }
}