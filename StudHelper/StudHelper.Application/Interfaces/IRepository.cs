﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace StudHelper.Application.Interfaces
{
    public interface IRepository<TEntity>
    {
        Task<bool> AddAsync(TEntity entity);

        Task<IList<TDto>> GetAsync<TDto>(
            Func<TEntity, bool> filter = null,
            int offset = 0,
            int size = -1)
            where TDto : class;

        Task<IList<TEntity>> GetAsync(
            Func<TEntity, bool> filter = null,
            int offset = 0,
            int size = -1);

        Task<TDto> GetFirstAsync<TDto>(
            Func<TEntity, bool> filter = null)
            where TDto : class;

        Task<TEntity> GetFirstAsync(
            Func<TEntity, bool> filter = null);

        Task<TDto> GetLastAsync<TDto>(
            Func<TEntity, bool> filter = null)
            where TDto : class;

        Task<TEntity> GetLastAsync(
            Func<TEntity, bool> filter = null);

        Task<bool> UpdateAsync(TEntity entity);

        Task<bool> UpdateRangeAsync(IEnumerable<TEntity> entities);

        Task<bool> RemoveAsync(TEntity entity);

        Task<int> CountAsync();

        Task<bool> SaveChangesAsync();
    }
}