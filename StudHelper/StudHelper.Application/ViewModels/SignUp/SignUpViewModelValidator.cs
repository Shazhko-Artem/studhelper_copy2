﻿using FluentValidation;
using StudHelper.Application.Resources;

namespace StudHelper.Application.ViewModels.SignUp
{
    public class SignUpViewModelValidator : AbstractValidator<SignUpViewModel>
    {
        public SignUpViewModelValidator()
        {
            this.RuleFor(x => x.Email).NotEmpty().EmailAddress().WithMessage(ValidationMessages.WrongEmailFormat);
            this.RuleFor(x => x.Password).NotEmpty();
            this.RuleFor(x => x.ConfirmPassword).Equal(x => x.Password).WithName(ValidationMessages.ConfirmPassword);
            this.RuleFor(x => x.FirstName).NotEmpty().WithName(ValidationMessages.FirstName);
            this.RuleFor(x => x.LastName).NotEmpty().WithName(ValidationMessages.LastName);
        }
    }
}