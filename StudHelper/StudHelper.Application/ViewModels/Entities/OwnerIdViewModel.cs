﻿namespace StudHelper.Application.ViewModels.Entities
{
    public class OwnerIdViewModel
    {
        public int OwnerId { get; set; }
    }
}