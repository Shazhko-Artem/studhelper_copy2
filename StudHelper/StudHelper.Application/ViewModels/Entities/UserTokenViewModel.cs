﻿namespace StudHelper.Application.ViewModels.Entities
{
    public class UserTokenViewModel
    {
        public UserTokenViewModel(int userId, string token)
        {
            this.UserId = userId;
            this.Token = token;
        }

        public int UserId { get; set; }

        public string Token { get; set; }
    }
}