﻿using System.Collections.Generic;

namespace StudHelper.Application.ViewModels.Utils
{
    public class ResultViewModel
    {
        public bool IsSucceeded { get; set; }

        public List<string> Errors { get; set; }
    }
}