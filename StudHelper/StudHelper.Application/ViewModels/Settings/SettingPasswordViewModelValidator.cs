﻿using FluentValidation;

namespace StudHelper.Application.ViewModels.Settings
{
    public class SettingPasswordViewModelValidator : AbstractValidator<SettingPasswordViewModel>
    {
        public SettingPasswordViewModelValidator()
        {
            this.RuleFor(x => x.OldPassword).NotEmpty();
            this.RuleFor(x => x.Password).NotEmpty();
            this.RuleFor(x => x.ConfirmPassword).Equal(x => x.Password);
        }
    }
}