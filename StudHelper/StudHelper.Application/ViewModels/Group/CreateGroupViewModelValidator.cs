﻿using FluentValidation;

namespace StudHelper.Application.ViewModels.Group
{
    public class CreateGroupViewModelValidator : AbstractValidator<CreateGroupViewModel>
    {
        public CreateGroupViewModelValidator()
        {
            this.RuleFor(x => x.Title).NotNull().NotEmpty().MaximumLength(60).MinimumLength(2);
            this.RuleFor(x => x.Description).NotNull().NotEmpty().MaximumLength(5120);
        }
    }
}