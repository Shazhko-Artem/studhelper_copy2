﻿using System.Collections.Generic;
using JW;

namespace StudHelper.Application.ViewModels.Group
{
    public class GroupListViewModel
    {
        public IList<GroupListItemViewModel> Groups { get; set; }

        public Pager Pager { get; set; }
    }
}