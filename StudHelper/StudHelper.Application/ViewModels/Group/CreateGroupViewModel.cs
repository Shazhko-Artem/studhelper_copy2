﻿namespace StudHelper.Application.ViewModels.Group
{
    public class CreateGroupViewModel
    {
        public int Id { get; set; }

        public int OwnerId { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }
    }
}