﻿using System.Collections.Generic;
using StudHelper.Application.ViewModels.Resource;
using StudHelper.Application.ViewModels.User;

namespace StudHelper.Application.ViewModels.Group
{
    public class GroupViewModel
    {
        public int Id { get; set; }

        public UserViewModel Owner { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public IList<ResourceViewModel> Resources { get; set; }
    }
}