﻿using System.Text.Encodings.Web;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.AspNetCore.Mvc;
using StudHelper.Application.Interfaces.Notifications;
using StudHelper.Application.Interfaces.Services;
using StudHelper.Application.ViewModels.PasswordRecovery;
using StudHelper.Application.ViewModels.SignIn;
using StudHelper.Application.ViewModels.SignUp;

namespace StudHelper.Web.Controllers
{
    public class AccountController : Controller
    {
        private readonly IAccountService accountService;
        private readonly IEmailSender emailSender;
        private readonly IMessageProvider messageProvider;

        public AccountController(IAccountService accountService, IEmailSender emailSender, IMessageProvider messageProvider)
        {
            this.accountService = accountService;
            this.emailSender = emailSender;
            this.messageProvider = messageProvider;
        }

        public IActionResult SignIn(string returnUrl = "/")
        {
            var model = new SignInViewModel() { ReturnUrl = returnUrl };
            return this.View(model);
        }

        [HttpPost]
        public async Task<IActionResult> SignIn(SignInViewModel model)
        {
            if (!this.ModelState.IsValid)
            {
                return this.View(model);
            }

            var result = await this.accountService.SignIn(model);
            if (result.IsSucceeded)
            {
                return this.Redirect(model.ReturnUrl);
            }
            else
            {
                this.ModelState.AddModelError(string.Empty, "Incorrect email or password");
                return this.View(model);
            }
        }

        public IActionResult SignUp()
        {
            return this.View();
        }

        [HttpPost]
        public async Task<IActionResult> SignUp(SignUpViewModel model)
        {
            if (!this.ModelState.IsValid)
            {
                return this.View(model);
            }

            var signUpResult = await this.accountService.SignUp(model);
            var userToken = signUpResult.Result;

            if (signUpResult.IsSucceeded)
            {
                var callbackUrl = this.Url.Action(
                    "ConfirmEmail",
                    "Account",
                    values: new { userId = userToken.UserId, token = userToken.Token },
                    protocol: this.Request.Scheme);

                string callBackUrl = HtmlEncoder.Default.Encode(callbackUrl);
                string message = this.messageProvider.GetMessage(EmailMessageType.EmailConfirmation, callBackUrl);
                await this.emailSender.SendEmailAsync(model.Email, "Email confirmation", message);
            }
            else
            {
                foreach (var error in signUpResult.Errors)
                {
                    this.ModelState.AddModelError(string.Empty, error);
                }

                return this.View(model);
            }

            return this.RedirectToAction("Index", "Home");
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> ConfirmEmail(string userId, string token)
        {
            if (userId == null || token == null)
            {
                return this.NotFound();
            }

            var result = await this.accountService.ConfirmEmailAsync(userId, token);

            if (result.IsSucceeded)
            {
                return this.View();
            }
            else
            {
                return this.NotFound();
            }
        }

        public IActionResult RequestPasswordRecovery()
        {
            return this.View();
        }

        [HttpPost]
        public async Task<IActionResult> RequestPasswordRecovery(RequestPasswordRecoveryViewModel model)
        {
            if (!this.ModelState.IsValid)
            {
                return this.View(model);
            }

            var passwordRecoveryResult = await this.accountService.RequestPasswordRecoveryAsync(model);
            var userToken = passwordRecoveryResult.Result;

            if (passwordRecoveryResult.IsSucceeded)
            {
                var callbackUrl = this.Url.Action(
                    "RecoverPassword",
                    "Account",
                    values: new { userId = userToken.UserId, token = userToken.Token },
                    protocol: this.Request.Scheme);

                string callBackUrl = HtmlEncoder.Default.Encode(callbackUrl);
                string message = this.messageProvider.GetMessage(EmailMessageType.PasswordRecovery, callBackUrl);
                await this.emailSender.SendEmailAsync(model.Email, "Password recovery", message);
            }
            else
            {
                foreach (var error in passwordRecoveryResult.Errors)
                {
                    this.ModelState.AddModelError(string.Empty, error);
                }

                return this.View(model);
            }

            return this.RedirectToAction("Index", "Home");
        }

        public IActionResult RecoverPassword(int userId, string token)
        {
            var model = new PasswordRecoveryViewModel(userId, token);

            return this.View(model);
        }

        [HttpPost]
        public async Task<IActionResult> RecoverPassword(PasswordRecoveryViewModel model)
        {
            if (!this.ModelState.IsValid)
            {
                return this.View(model);
            }

            var passwordRecoveryResult = await this.accountService.ResetPasswordAsync(model);

            if (!passwordRecoveryResult.IsSucceeded)
            {
                foreach (var error in passwordRecoveryResult.Errors)
                {
                    this.ModelState.AddModelError(string.Empty, error);
                }

                return this.View(model);
            }

            return this.RedirectToAction("SignIn");
        }
    }
}