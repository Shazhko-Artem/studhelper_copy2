﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using StudHelper.Application.Interfaces.Services;
using StudHelper.Application.ViewModels.Settings;
using StudHelper.Web.Utils;

namespace StudHelper.Web.Controllers
{
    [Authorize]
    public class SettingsController : Controller
    {
        private readonly IUserService userService;

        public SettingsController(IUserService userService)
        {
            this.userService = userService;
        }

        public async Task<IActionResult> Profile()
        {
            int userId = this.User.GetUserId();

            var resultViewModel = await this.userService.GetUserAsync(u => u.Id == userId);

            if (!resultViewModel.IsSucceeded)
            {
                return this.StatusCode(500);
            }

            var model = new SettingProfileViewModel(resultViewModel.Result);
            return this.View(model);
        }

        [HttpPost]
        public async Task<IActionResult> Profile(SettingProfileViewModel model)
        {
            model.Done = false;

            if (!this.ModelState.IsValid)
            {
                return this.View(model);
            }

            model.UserId = this.User.GetUserId();
            var result = await this.userService.UpdateAsync(this.User.GetUserId(), model);

            if (!result.IsSucceeded)
            {
                foreach (var error in result.Errors)
                {
                    this.ModelState.AddModelError(string.Empty, error);
                }
            }
            else
            {
                model.Done = true;
            }

            return this.View(model);
        }

        public async Task<IActionResult> Password()
        {
            int userId = this.User.GetUserId();
            var resultViewModel = await this.userService.GetUserAsync(u => u.Id == userId);

            if (!resultViewModel.IsSucceeded)
            {
                return this.StatusCode(500);
            }

            var model = new SettingPasswordViewModel(resultViewModel.Result);
            return this.View(model);
        }

        [HttpPost]
        public async Task<IActionResult> Password(SettingPasswordViewModel model)
        {
            model.Done = false;

            if (!this.ModelState.IsValid)
            {
                return this.View(model);
            }

            model.UserId = this.User.GetUserId();

            var result = await this.userService.ChangePasswordAsync(model);

            if (!result.IsSucceeded)
            {
                foreach (var error in result.Errors)
                {
                    this.ModelState.AddModelError(string.Empty, error);
                }
            }
            else
            {
                model.Done = true;
            }

            return this.View(model);
        }
    }
}