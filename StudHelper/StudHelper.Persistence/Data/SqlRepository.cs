﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using StudHelper.Application.Interfaces;

namespace StudHelper.Persistence.Data
{
    public class SqlRepository<TEntity> : IRepository<TEntity>
        where TEntity : class
    {
        private readonly StudHelperDbContext context;
        private readonly DbSet<TEntity> dbSet;
        private readonly IMapper mapper;
        private readonly ILogger<SqlRepository<TEntity>> logger;

        public SqlRepository(StudHelperDbContext context, IMapper mapper, ILogger<SqlRepository<TEntity>> logger)
        {
            this.context = context;
            this.dbSet = this.context.Set<TEntity>();
            this.mapper = mapper;
            this.logger = logger;
        }

        public async Task<bool> AddAsync(TEntity entity)
        {
            await this.dbSet.AddAsync(entity);
            return true;
        }

        public async Task<IList<TDto>> GetAsync<TDto>(
            Func<TEntity,
            bool> filter = null,
            int offset = 0,
            int size = -1)
            where TDto : class
        {
            IQueryable<TEntity> query = this.dbSet;
            if (filter != null)
            {
                query = query.Where(item => filter(item));
            }

            if (offset > 0)
            {
                query = query.Skip(offset);
            }

            if (size > 0)
            {
                query = query.Take(size);
            }

            var result = await this.mapper.ProjectTo<TDto>(query).ToListAsync();

            return result;
        }

        public async Task<IList<TEntity>> GetAsync(
           Func<TEntity,
           bool> filter = null,
           int offset = 0,
           int size = -1)
        {
            IQueryable<TEntity> query = this.dbSet;
            if (filter != null)
            {
                query = query.Where(item => filter(item));
            }

            if (offset > 0)
            {
                query = query.Skip(offset);
            }

            if (size > 0)
            {
                query = query.Take(size);
            }

            var result = await query.ToListAsync();

            return result;
        }

        public async Task<TDto> GetFirstAsync<TDto>(Func<TEntity, bool> filter = null)
            where TDto : class
        {
            IQueryable<TEntity> query = this.dbSet;

            TDto entity = null;

            if (filter != null)
            {
                entity = await this.mapper.ProjectTo<TDto>(query.Where(item => filter(item))).FirstOrDefaultAsync();
            }
            else
            {
                entity = await this.mapper.ProjectTo<TDto>(query).FirstOrDefaultAsync();
            }

            return entity;
        }

        public async Task<TEntity> GetFirstAsync(Func<TEntity, bool> filter = null)
        {
            IQueryable<TEntity> query = this.dbSet;

            TEntity entity = null;

            if (filter != null)
            {
                entity = await query.FirstOrDefaultAsync(item => filter(item));
            }
            else
            {
                entity = await query.FirstOrDefaultAsync();
            }

            return entity;
        }

        public async Task<TDto> GetLastAsync<TDto>(Func<TEntity, bool> filter = null)
            where TDto : class
        {
            IQueryable<TEntity> query = this.dbSet;

            TDto entity = null;

            if (filter != null)
            {
                entity = await this.mapper.ProjectTo<TDto>(query.Where(item => filter(item))).LastOrDefaultAsync();
            }
            else
            {
                entity = await this.mapper.ProjectTo<TDto>(query).LastOrDefaultAsync();
            }

            return entity;
        }

        public async Task<TEntity> GetLastAsync(Func<TEntity, bool> filter = null)
        {
            IQueryable<TEntity> query = this.dbSet;

            TEntity entity = null;

            if (filter != null)
            {
                entity = await query.LastOrDefaultAsync(item => filter(item));
            }
            else
            {
                entity = await query.LastOrDefaultAsync();
            }

            return entity;
        }

        public Task<bool> UpdateAsync(TEntity entity)
        {
            this.dbSet.Update(entity);
            return Task.FromResult(true);
        }

        public Task<bool> UpdateRangeAsync(IEnumerable<TEntity> entities)
        {
            this.dbSet.UpdateRange(entities);
            return Task.FromResult(true);
        }

        public Task<bool> RemoveAsync(TEntity entity)
        {
            this.dbSet.Remove(entity);
            return Task.FromResult(true);
        }

        public Task<int> CountAsync()
        {
            return this.dbSet.CountAsync();
        }

        public async Task<bool> SaveChangesAsync()
        {
            try
            {
                await this.context.SaveChangesAsync();
            }
            catch (Exception exception)
            {
                this.logger.LogError($"Time: {DateTime.Now}{Environment.NewLine}" +
                                     $"Exception: {exception.Message}");
                return await Task.FromResult(false);
            }

            return await Task.FromResult(true);
        }
    }
}