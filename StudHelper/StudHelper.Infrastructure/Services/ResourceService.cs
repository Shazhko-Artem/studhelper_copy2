﻿using System;
using System.Threading.Tasks;
using AutoMapper;
using StudHelper.Application.Interfaces;
using StudHelper.Application.Interfaces.Services;
using StudHelper.Application.ViewModels.Entities;
using StudHelper.Application.ViewModels.Resource;
using StudHelper.Application.ViewModels.Utils;
using StudHelper.Domain.Entities;
using StudHelper.Infrastructure.Utils;

namespace StudHelper.Infrastructure.Services
{
    public class ResourceService : IResourceService
    {
        private readonly IRepository<Resource> resourceRepository;
        private readonly IRepository<Group> groupRepository;
        private readonly IMapper mapper;

        public ResourceService(IRepository<Resource> resourceRepository, IRepository<Group> groupRepository, IMapper mapper)
        {
            this.resourceRepository = resourceRepository;
            this.groupRepository = groupRepository;
            this.mapper = mapper;
        }

        public async Task<ResultViewModel> AddAsync(int userId, ResourceViewModel resource)
        {
            int groupId = resource.GroupId;
            var owner = await this.groupRepository.GetFirstAsync<OwnerIdViewModel>(r => r.Id == groupId);

            if (owner.OwnerId != userId)
            {
                return new ResultViewModel() { IsSucceeded = false, Errors = ErrorExtension.GetErrorListFrom("Access denied") };
            }

            Resource newResource = this.mapper.Map<Resource>(resource);
            if (!await this.resourceRepository.AddAsync(newResource))
            {
                return new ResultViewModel() { IsSucceeded = false, Errors = ErrorExtension.GetErrorListFrom("Add resource error") };
            }

            if (!await this.resourceRepository.SaveChangesAsync())
            {
                return new ResultViewModel() { IsSucceeded = false, Errors = ErrorExtension.GetErrorListFrom("Failed to save changes") };
            }

            return new ResultViewModel() { IsSucceeded = true };
        }

        public Task<ResultViewModel> UpdateAsync(int userId, ResourceViewModel resource)
        {
            throw new NotImplementedException(); 
        }
    }
}